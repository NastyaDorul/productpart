import React from "react";
import { Link } from "react-router-dom";
import "./style.scss";

const ListItemComponent = (props) => {
  return (
    <Link
      to={`/category?parentCategory=${props.categoryName
        .toLowerCase()
        .split(" ")
        .join("")}&subCategory=${props.array}`}
    >
      <div
        className="dropdown-item"
        data-array={props.array}
        data-parent={props.categoryName}
        onClick={(e, subCategory, parentCategory) => {
          props.onSubCategoryChanged(e.target.dataset.array, subCategory);
          props.onParentCategoryChanged(
            e.target.dataset.parent,
            parentCategory
          );
        }}
      >
        {props.array}
      </div>
    </Link>
  );
};
export default ListItemComponent;
