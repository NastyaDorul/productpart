import React from "react";
import ListItemComponent from "./ListItem";

const ListComponent = (props, { listHeader }) => {
  let listItems = props.listItems.map((elem, index) => {
    return (
      <div key={index}>
        <ListItemComponent
          array={elem}
          linkItems={props.listLink}
          onParentCategoryChanged={props.onParentCategoryChanged}
          onSubCategoryChanged={props.onSubCategoryChanged}
          categoryName={props.categoryName}
        />
      </div>
    ); //сделать переборку массива
  });

  return (
    <ul>
      {listHeader && <p>{listHeader}</p>}
      {listItems}
    </ul>
  );
};
export default ListComponent;
