import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import products from "./products";
import search from "./searchForm";
import result from "./Results";

const rootReducer = combineReducers({
  products: products,
  search: search,
  result: result,
  form: formReducer,
  //nul: null
});
export default rootReducer;
