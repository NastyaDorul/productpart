const InitialState = {
  result: null,
  isReady: false,
  totalProductCount: null,
  currentPage: 1,
  limit: 12, //quantity of rendering
  selectedTypeOfSort: "SortBy is Empty",
  selectedColorFilter: "ColorFilter is Empty",
  selectedSizeFilter: "SizeFilter is Empty",
  selectedRange: {
    "20 to 49.99": false,
    "50 to 99.99": false,
    "100 to 499.99": false,
  },
  categoryPriceMin: "do not set",
  categoryPriceMax: "do not set",

  sortingItems: null,
  isCategorySorting: false,
  sortingColorItems: null,
  isColorSorting: false,
  sortingSizeItems: null,
  isSizeSorting: false,
  sortingPriceItems: null,
  isPriceSorting: false,
  sortingBrandItems: null,
  isBrandSorting: false,
  sortingGSItems: null,
  isGS: false, //Gaming System

  selectedBrand: {
    Midway: false,
    Atari: false,
    LucasArts: false,
    Namco: false,
    Nintendo: false,
    EASports: false,
    Sega: false,
    "Ubi Soft": false,
    "Rockstar Games": false,
    Sierra: false,
    Sony: false,
    Microsoft: false,
    No: false,
  },
  selectedGS: {
    "Sony PSP": false,
    Wii: false,
    "Nintendo DS": false,
    SonyPS3: false,
    "X-Box 360": false,
    No: false,
  },

  parentCategory: "",
  subCategory: "",

  currentItemDetails: {},
  itemDetails: "empty",
  itemDetailsIsReady: false,
};
export default (state = InitialState, action) => {
  switch (action.type) {
    case "SET_SEARCH_RESULT":
      return {
        ...state,
        result: action.payload,
        isReady: true,
      };
    case "SET_CURRENT_PAGE":
      return {
        ...state,
        currentPage: action.payload,
      };
    case "SET_PAGE_SIZE":
      return {
        ...state,
        pageSize: action.payload,
      };

    case "SET_TOTAL_RPODUCT_COUNT":
      return {
        ...state,
        totalProductCount: action.payload,
      };
    case "SET_PAGE_LIMIT":
      return {
        ...state,
        limit: action.payload,
      };
    case "SET_CURRENT_TYPE_OF_SORT":
      return {
        ...state,
        selectedTypeOfSort: action.payload,
      };
    case "SET_COLOR_FILTER":
      return {
        ...state,
        selectedColorFilter: action.payload,
      };
    case "SET_SIZE_FILTER":
      return {
        ...state,
        selectedSizeFilter: action.payload,
      };

    case "SET_PRICE_RANGE":
      return {
        ...state,
        selectedRange: action.payload,
      };
    case "SET_BRAND_FILTER":
      return {
        ...state,
        selectedBrand: action.payload,
      };
    case "SET_GAMING_SYSTEM_FILTER":
      return {
        ...state,
        selectedGS: action.payload,
      };

    case "SET_SUB_CATEGORY":
      return {
        ...state,
        subCategory: action.payload,
      };
    case "SET_PARENT_CATEGORY":
      return {
        ...state,
        parentCategory: action.payload,
      };
    case "SET_MIN":
      return {
        ...state,
        categoryPriceMin: action.payload,
      };
    case "SET_MAX":
      return {
        ...state,
        categoryPriceMax: action.payload,
      };
    case "SET_DETAILS_OF_ITEM":
      return {
        ...state,
        currentItemDetails: action.payload,
      };
    case "SET_ITEM":
      return {
        ...state,
        itemDetails: action.payload,
        itemDetailsIsReady: true,
      };
    case "SET_SORTING_ITEMS":
      return {
        ...state,
        sortingItems: action.payload,
        isCategorySorting: true,
      };
    case "SET_SORTING_COLOR":
      return {
        ...state,
        sortingColorItems: action.payload,
        isColorSorting: true,
      };
    case "SET_SORTING_SIZE":
      return {
        ...state,
        sortingSizeItems: action.payload,
        isSizeSorting: true,
      };
    case "SET_SORTING_PRICE":
      return {
        ...state,
        sortingPriceItems: action.payload,
        isPriceSorting: true,
      };
    case "SET_SORTING_BRAND":
      return {
        ...state,
        sortingBrandItems: action.payload,
        isBrandSorting: true,
      };
    case "SET_SORTING_GS":
      return {
        ...state,
        sortingGSItems: action.payload,
        isGS: true,
      };

    default:
      return state;
  }
};
