export const setSearchResult = (result) => ({
  type: "SET_SEARCH_RESULT",
  payload: result,
});
export const setCurrentPage = (currentPage) => ({
  type: "SET_CURRENT_PAGE",
  payload: currentPage,
});
export const setCurrentPageResult = (pageResult) => ({
  type: "CURRENT_PAGE_RESULT",
  payload: pageResult,
});
export const setPageLimit = (limit) => ({
  type: "SET_PAGE_LIMIT",
  payload: limit,
});
export const setTotalProductCount = (total) => ({
  type: "SET_TOTAL_RPODUCT_COUNT",
  payload: total,
});
export const setCurrentTypeOfSort = (selectedTypeOfSort) => ({
  type: "SET_CURRENT_TYPE_OF_SORT",
  payload: selectedTypeOfSort,
});
export const setColorFilter = (selectedColorFilter) => ({
  type: "SET_COLOR_FILTER",
  payload: selectedColorFilter,
});
export const setSizeFilter = (selectedSizeFilter) => ({
  type: "SET_SIZE_FILTER",
  payload: selectedSizeFilter,
});

export const setPriceRange = (selectedRange) => ({
  type: "SET_PRICE_RANGE",
  payload: selectedRange,
});
export const setBrandFilter = (selectedBrand) => ({
  type: "SET_BRAND_FILTER",
  payload: selectedBrand,
});
export const setGSFilter = (selectedGS) => ({
  type: "SET_GAMING_SYSTEM_FILTER",
  payload: selectedGS,
});

export const setSubCategory = (subCategory) => ({
  type: "SET_SUB_CATEGORY",
  payload: subCategory,
});
export const setParentCategory = (parentCategory) => ({
  type: "SET_PARENT_CATEGORY",
  payload: parentCategory,
});
export const setMin = (categoryPriceMin) => ({
  type: "SET_MIN",
  payload: categoryPriceMin,
});
export const setMax = (categoryPriceMax) => ({
  type: "SET_MAX",
  payload: categoryPriceMax,
});
export const setItemIdTitlePriceImg = (currentItemDetails) => ({
  type: "SET_DETAILS_OF_ITEM",
  payload: currentItemDetails,
});
export const setItemDetails = (itemDetails) => ({
  type: "SET_ITEM",
  payload: itemDetails,
});
export const setSortingItems = (sortingItems) => ({
  type: "SET_SORTING_ITEMS",
  payload: sortingItems,
});
export const setSortingColor = (sortingColorItems) => ({
  type: "SET_SORTING_COLOR",
  payload: sortingColorItems,
});
export const setSortingSize = (sortingSizeItems) => ({
  type: "SET_SORTING_SIZE",
  payload: sortingSizeItems,
});
export const setSortingPrice = (sortingPriceItems) => ({
  type: "SET_SORTING_PRICE",
  payload: sortingPriceItems,
});
export const setSortingBrand = (sortingBrandItems) => ({
  type: "SET_SORTING_BRAND",
  payload: sortingBrandItems,
});
export const setSortingGS = (sortingGSItems) => ({
  type: "SET_SORTING_GS",
  payload: sortingGSItems,
});
