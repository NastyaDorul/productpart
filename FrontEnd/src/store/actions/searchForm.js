export const setSearchQuery = (searchText) => ({
  type: "SET_SEARCH_QUERY",
  payload: searchText,
});
export const setSearchQueryResult = (searchResult) => ({
  type: "SET_SEARCH_QUERY_RESULT",
  payload: searchResult,
});
