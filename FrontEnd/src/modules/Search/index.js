import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

import Button from "../../components/Button";
import Input from "../../components/Input";
import { connect } from "react-redux";
import "./style.scss";
class SearchComponent extends Component {
  state = {
    searchText: "",
  };
  HandelChange = (ev) => {
    const state = this.state;
    state.searchText = ev.target.value;
    this.setState(state);
  };
  onClick = () => {
    console.log(this.state.searchText);
  };
  render() {
    return (
      <div className="form-inline my-2 my-lg-0">
        <Input
          className="ownstyle-input mr-sm-2"
          onChange={this.HandelChange}
          placeHolder="Enter Keyword or Item No."
          type="search"
          aria-label="Search"
        />

        <Button
          className="ownstyle-button my-2 my-sm-0"
          type="submit"
          onClick={this.onClick}
          text={<FontAwesomeIcon icon={faSearch} style={{ color: "grey" }} />}
        />
      </div>
    );
  }
}
const mapDispatchToProps = (dispatch) => ({});
export default connect(null, null)(SearchComponent);
