import React from "react";
import axios from "axios";
import { connect } from "react-redux";

import NavbarComponent from "../component/index";
import {
  setSubCategory,
  setParentCategory,
  setSortingItems,
  setSortingColor,
  setSortingSize,
  setSortingPrice,
  setSortingBrand,
  setSortingGS,
} from "../../../store/actions/Results";

function handleErrors(err) {
  if (err.response) {
    console.log("Error with response", err.response.status);
  } else if (err.request) {
    console.log("Error with request");
  } else {
    console.log("Error", err.message);
  }
}
class NavbarContainer extends React.Component {
  componentDidMount() {
    const { parentCategory, subCategory } = this.props;
    console.log(parentCategory, subCategory);
  }

  onSubCategoryChanged = (subCategory) => {
    this.props.setSubCategory(subCategory);
  };

  onParentCategoryChanged = (parentCategory) => {
    this.props.setParentCategory(parentCategory);
    console.log(parentCategory);
    axios
      .get(
        `http://localhost:5000/api/products/category/${parentCategory
          .split(" ")
          .join("")}/sorting`
      )
      .then((res) => {
        let categories = res.data.find((item) => item.name == "subcategory");
        if (categories) {
          this.props.setSortingItems(categories.sortingvalues);
          console.log(categories.sortingvalues);
        } else {
          this.props.setSortingItems(null);
          console.log("this category does'not contain subcategory filter");
        }

        let selectedbrand = res.data.find(
          (item) => item.name == "selectedbrand"
        );
        if (selectedbrand) {
          this.props.setSortingBrand(selectedbrand.sortingvalues);
          console.log(selectedbrand.sortingvalues);
        } else {
          this.props.setSortingBrand(null);
          console.log("this category does'not contain selectedbrand filter");
        }

        let selectedsize = res.data.find((item) => item.name == "selectedsize");
        if (selectedsize) {
          this.props.setSortingSize(selectedsize.sortingvalues);
          console.log(selectedsize.sortingvalues);
        } else {
          this.props.setSortingSize(null);
          console.log("this category does'not contain selectedsize filter");
        }
        let selectedcolor = res.data.find(
          (item) => item.name == "selectedcolor"
        );
        if (selectedcolor) {
          this.props.setSortingColor(selectedcolor.sortingvalues);
        } else {
          this.props.setSortingColor(null);
          console.log("this category does'not contain selectedcolor filter");
        }

        let selectedgs = res.data.find((item) => item.name == "selectedgs");
        if (selectedgs) {
          this.props.setSortingGS(selectedgs.sortingvalues);
        } else {
          this.props.setSortingGS(null);
          console.log("this category does'not contain sortingGSItems filter");
        }
      })

      .catch(handleErrors);
  };

  sortNewArrivals = ["subcategory", "selectedprice"];
  sortArrayElectronics = [
    "subcategory",
    "selectedbrand",
    "selectedgs",
    "selectedprice",
  ];
  sortArrayWomans = [
    "subcategory",
    "selectedbrand",
    "selectedcolor",
    "selectedsize",
    "selectedprice",
  ];
  sortArrayMens = [
    "subcategory",
    "selectedbrand",
    "selectedcolor",
    "selectedsize",
    "selectedprice",
  ];
  sortTopSellers = ["subcategory", "selectedprice"];

  NavbarArray = [
    {
      name: "new arrivals",
      arr: ["womans", "mans", "electronics"],
    },
    {
      name: "womans",
      arr: [
        "clothing",
        "outfits",
        "tops",
        "dresses",
        "bottoms",
        "jackets & coats",
        "feeling red",
        "jewelry",
        "earings",
        "bracelets",
        "necklaces",
        "accessories",
        "scarves",
        "shoes",
      ],
    },
    {
      name: "mens",
      arr: [
        "clothing",
        "suits",
        "jackets & coats",
        "dress shirts",
        "shorts",
        "pants",
        "accessories",
        "ties",
        "gloves",
        "luggage",
      ],
    },
    {
      name: "electronics",
      arr: [
        "televisions",
        "digital cameras",
        "ipods & mp3 players",
        "gps navigations",
        "gaming",
      ],
    },
    { name: "top sellers", arr: ["top sellers"] },
  ];
  render() {
    return (
      <NavbarComponent
        NavbarArray={this.NavbarArray}
        onParentCategoryChanged={this.onParentCategoryChanged}
        onSubCategoryChanged={this.onSubCategoryChanged}
      />
    );
  }
}

const mapStateToProps = ({ result }) => ({
  subCategory: result.subCategory,
  parentCategory: result.parentCategory,
  sortingItems: result.sortingItems,
  sortingColorItems: result.sortingColorItems,
  sortingSizeItems: result.sortingSizeItems,
  sortingPriceItems: result.sortingPriceItems,
  sortingBrandItems: result.sortingBrandItems,
  sortingGSItems: result.sortingGSItems,
});
const mapDispatchToProps = (dispatch) => ({
  setSubCategory: (subCategory) => {
    dispatch(setSubCategory(subCategory));
  },
  setParentCategory: (parentCategory) => {
    dispatch(setParentCategory(parentCategory));
  },
  setSortingItems: (sortingItems) => {
    dispatch(setSortingItems(sortingItems));
  },
  setSortingColor: (sortingColorItems) => {
    dispatch(setSortingColor(sortingColorItems));
  },
  setSortingSize: (sortingSizeItems) => {
    dispatch(setSortingSize(sortingSizeItems));
  },
  setSortingBrand: (sortingBrandItems) => {
    dispatch(setSortingBrand(sortingBrandItems));
  },
  setSortingPrice: (sortingPriceItems) => {
    dispatch(setSortingPrice(sortingPriceItems));
  },
  setSortingGS: (sortingGSItems) => {
    dispatch(setSortingGS(sortingGSItems));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(NavbarContainer);
