import React from "react";
import { Link } from "react-router-dom";
import ListComponent from "../../../components/List";
import HoverComponent from "../../../components/Hover";

const NavbarItemComponent = (props) => {
  return (
    <li className={props.className}>
      <HoverComponent
        onHover={
          <ListComponent
            listItems={props.categoryItems}
            onParentCategoryChanged={props.onParentCategoryChanged}
            onSubCategoryChanged={props.onSubCategoryChanged}
            categoryName={props.categoryName}
          />
        }
      >
        <Link
          to={`/parentCategory?${props.categoryName.toLowerCase()}`}
          onClick={(e, parentCategory, subCategory) => {
            props.onParentCategoryChanged(
              e.target.innerText.toLowerCase(),
              parentCategory
            );
            props.onSubCategoryChanged("", subCategory);
          }}
        >
          {props.categoryName}
        </Link>
        {/*добавила Link */}
        <div></div>
      </HoverComponent>
    </li>
  );
};
export default NavbarItemComponent;
