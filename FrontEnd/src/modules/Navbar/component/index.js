import React from "react";
import NavbarItemComponent from "../NavbarItem";
import "../style.scss";

const NavbarComponent = (props) => {
  let NavbarItems = props.NavbarArray.map((elem, index) => {
    return (
      <NavbarItemComponent
        key={index}
        categoryName={elem.name.toUpperCase()}
        categoryItems={elem.arr}
        className="navbar-item"
        onParentCategoryChanged={props.onParentCategoryChanged}
        onSubCategoryChanged={props.onSubCategoryChanged}
      />
    );
  });
  return <nav className="navbar">{NavbarItems}</nav>;
};
export default NavbarComponent;
