//React
import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
//Redux
import { Provider } from "react-redux";
import store from "./store/";
//Pages
import HomePage from "./pages/Home";
import ResultsConsoles from "./pages/ResultsConsoles/containers/ResultsConsoles";
import ItemsConsoles from "./pages/ItemsConsoles/containers/ItemsConsoles";
import ItemsDetalizationContainer from "./pages/ItemsDetalization/containers/ItemsDetalization";
import ResultContainer from "./pages/Results/container/Results";

//reset.scss

import {
  faMapMarkerAlt,
  faUserAlt,
  faShoppingCart,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import NavbarContainer from "../src/modules/Navbar/container/Navbar";

import SearchComponent from "../src/modules/Search/index"; //not my search
import ItemsSearchForm from "../src/pages/ItemsSearchForm/container/ItemSearchForm";
import Container from "../src/components/Container/index";

import Logo from "../src/components/Logo/index";

import "./_reset.scss";
import WellcomePageProfile from "./pages/WellcomePageProfile/container/WellcomePageProfile";
import WellcomePageItem from "./pages/WellcomePageProfile/container/WellcomePageItem";

const App = (props) => {
  console.log(props);
  return (
    <Provider store={store}>
      <Router>
        <Container>
          <Logo />
          <SearchComponent />
          <FontAwesomeIcon
            icon={faMapMarkerAlt}
            size="2x"
            style={{ color: "grey" }}
          />
          <FontAwesomeIcon
            icon={faUserAlt}
            size="2x"
            style={{ color: "grey" }}
          />
          <FontAwesomeIcon
            icon={faShoppingCart}
            size="2x"
            style={{ color: "grey" }}
          />
        </Container>
        <Container>
          <NavbarContainer />
        </Container>

        <Switch>
          {/*there are should be Home Page */}
          <Route exact path="/" render={() => <ResultsConsoles />} />
          {/*route for start product list */}
          <Route
            exact
            path="/parentCategory/"
            render={() => <ResultContainer />}
          />
          {/*<Results />*/}
          {/*route for detail info of product item */}
          <Route
            exact
            path="/products/gaming-Game-consoles/:id"
            render={() => <ItemsConsoles />}
          />
          {/*route for detail info of product item*/}
          <Route
            exact
            path="/products/detalization/:id"
            render={() => <ItemsDetalizationContainer />}
          />

          {/*route for detail info of product item*/}
          <Route
            exact
            path="/products/subCategory/"
            render={() => <WellcomePageItem />}
          />

          {/*route for search result page*/}
          <Route
            exact
            path="/search/products/:id"
            render={() => <ItemsSearchForm />}
          />

          {/*route for Wellcome Page*/}
          <Route path="/category/" render={() => <WellcomePageProfile />} />

          {/*route for Cart Page*/}
          <Route exact path="/cart" />
        </Switch>
      </Router>
    </Provider>
  );
};
export default App;
