import { connect } from "react-redux";
import { setFilter } from "../../../store/actions/filter";
import Filter from "../components/Filter";

const mapStateToProps = ({ products }) => {
  filterBy: products.filterBy;
};

const mapDispatchToProps = dispatch => {
  setFilter: filter => dispatch(setFilter(filter));
};

export default connect(mapStateToProps, mapDispatchToProps)(Filter);
