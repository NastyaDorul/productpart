import React from "react";
import { Link } from "react-router-dom";

const Card = (props) => {
  console.log(props);
  return (
    <div
      className="card-deck border shadow p-4 mb-4 bg-white"
      style={{ width: "18rem" }}
    >
      <div>
        <img src={props.link} alt="" className="card-img-top" />
      </div>
      <div className="card-body card-text-center text-dark ">
        <Link
          to={`/products/gaming-Game-consoles/${props.id}`}
          className="btn stretched-link"
        >
          <h4 className="card-title text-success" style={{ fontSize: "1rem" }}>
            {props.title}
          </h4>
        </Link>
        <h6 className="card-text text-secondary"> {props.price}</h6>
      </div>
    </div>
  );
};

export default Card;
