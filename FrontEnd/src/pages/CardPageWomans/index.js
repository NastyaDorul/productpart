import React from "react";
import { Link } from "react-router-dom";

const CardWomans = (props) => {
  return (
    <div
      style={{ width: "20rem" }}
      data-title={props.title}
      data-img={props.link}
      data-price={props.price}
      data-id={props.id}
      onClick={(e) => {
        let object = Object.assign(props.currentItemDetails);
        object.id = e.currentTarget.dataset.id;
        object.img = e.currentTarget.dataset.img;
        object.price = e.currentTarget.dataset.price;
        object.title = e.currentTarget.dataset.title;
        props.onItemClick(object);
      }}
    >
      <Link
        to={`/products/detalization/${props.id}`}
        className="btn stretched-link"
      >
        <div className=" d-flex  align-items-end justify-content-center card-deck border shadow m-0 p-0 ">
          <div className="bg-light">
            <img src={props.link} alt="" className=" card-img-top" />
          </div>
          <div
            className="d-flex justify-content-between card-body text-dark m-0 pl-2 pr-2 bg-dark"
            style={{ height: "89.77px" }}
          >
            <div>
              <h4
                className="card-title text-left  text-success"
                style={{ fontSize: "1rem" }}
              >
                {props.title}
              </h4>
            </div>

            <div>
              <h6 className="card-text text-right text-warning">
                {" "}
                {props.price}
              </h6>
            </div>
          </div>{" "}
        </div>
      </Link>
    </div>
  );
};

export default CardWomans;
