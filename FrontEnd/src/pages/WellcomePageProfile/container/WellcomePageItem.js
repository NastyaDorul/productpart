import React from "react";
import { connect } from "react-redux";
import WellcomePageItem from "../component/WellcomePageItem";
import "../style.scss";

class WellcomePageItemContainer extends React.Component {
  componentDidMount() {}

  render() {
    const { parentCategory, subCategory } = this.props;
    return (
      <div className="m-5 p-5 container d-flex justify-content-start">
        <WellcomePageItem
          subCategory={subCategory}
          parentCategory={parentCategory}
        />
      </div>
    );
  }
}

const mapStateToProps = ({ result }) => ({
  parentCategory: result.parentCategory,
  subCategory: result.subCategory,
});
const mapDispatchToProps = (dispatch) => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WellcomePageItemContainer);
