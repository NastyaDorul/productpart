import React from "react";
import { Link } from "react-router-dom";

const WellcomePageProfile = ({ parentCategory, subCategory }) => {
  console.log(parentCategory, subCategory);
  return (
    <div style={{ marginTop: "50px", marginBottom: "50px" }}>
      <div>
        <Link to="/">
          <h6 className="breadcrumbs">Home</h6>
        </Link>
        /{" "}
        {parentCategory !== "" ? (
          <div style={{ display: "inline" }}>
            {" "}
            <Link
              to={`/parentCategory?${parentCategory.toLowerCase()}`}
              className="breadcrumbs"
              //тут потрібно очистити сабкатегорію
            >
              {parentCategory.toLowerCase()}
            </Link>
            /{" "}
          </div>
        ) : (
          <></>
        )}
      </div>

      {parentCategory !== "" ? (
        <div>
          <h4>{parentCategory.toUpperCase()}</h4>
        </div>
      ) : (
        <></>
      )}
    </div>
  );
};

export default WellcomePageProfile;
