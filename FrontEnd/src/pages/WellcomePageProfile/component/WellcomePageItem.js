import React from "react";
import { Link } from "react-router-dom";
import WellcomePageSidebar from "./WellcomePageSidebar";
import WellcomePageInfo from "./WellcomePageInfo";

const WellcomePageItem = ({ parentCategory, subCategory }) => {
  return (
    <div style={{ marginTop: "50px", marginBottom: "50px" }}>
      <div>
        <Link to="/">
          <h6 className="breadcrumbs">Home</h6>
        </Link>
        /{" "}
        {parentCategory !== "" ? (
          <div style={{ display: "inline" }}>
            {" "}
            <Link
              to={`/parentCategory?${parentCategory.toLowerCase()}`}
              className="breadcrumbs"
              //тут потрібно очистити сабкатегорію
            >
              {parentCategory.toLowerCase()}
            </Link>
            /{" "}
          </div>
        ) : (
          <></>
        )}
        {subCategory !== "" ? (
          <div style={{ display: "inline" }}>
            <Link
              to={`/products/subCategory?${subCategory}`}
              className="breadcrumbs"
            >
              {subCategory}
            </Link>
            /{" "}
          </div>
        ) : (
          <></>
        )}
      </div>
      <div className="profilename">{subCategory}</div>
      {subCategory !== "" ? (
        <div>
          <WellcomePageSidebar />
          <WellcomePageInfo />
        </div>
      ) : (
        <></>
      )}
    </div>
  );
};
export default WellcomePageItem;
