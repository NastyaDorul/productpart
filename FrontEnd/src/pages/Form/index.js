import React from "react";
import axios from "axios";
import { reduxForm, Field, formValueSelector } from "redux-form";
import { connect } from "react-redux";

let SelectingFormValuesForm = (props) => {
  const {
    selectedTypeOfSort,
    quantityOfRendering,
    handleSubmit,
    ...otherProps
  } = props;
  console.log(selectedTypeOfSort, otherProps);

  const submitToServerSort = (event, selectedTypeOfSort) => {
    console.log(selectedTypeOfSort);
    axios
      .get(
        `http://localhost:5000/api/products/category/electronics/gaming-Games-sort?sortBy=${selectedTypeOfSort}`
      )
      .then((res) => {
        console.log(res);
      });
  };

  return (
    <form>
      <div>
        <Field name="sortBy" component="select" onChange={submitToServerSort}>
          <option value="">Select sort displaying...</option>
          <option value="Price Hight To Low">Price Hight To Low</option>
          <option value="Price Low To Hight">Price Low To Hight</option>
          <option value="Best Matches">Best Matches</option>
          <option value="Most Popular">Most Popular</option>
          <option value="Brand">Brand</option>
          <option value="Top Sellers">Top Sellers</option>
          <option value="Best Matches">Best Matches</option>
          <option value="Product Name A-Z">Product Name A-Z</option>
          <option value="Product Name Z-A">Product Name Z-A</option>
        </Field>
      </div>
      {/* {selectedTypeOfSort && (
        <div>
          <h6>Results of sort by {selectedTypeOfSort} </h6>
        </div>
      )}*/}
      <div>
        <Field
          name="quantityOfRendering"
          component="select"
          onChange={submitToServerSort}
        >
          <option value="12">12</option>
          <option value="24">24</option>
          <option value="36">36</option>
          <option value="48">48</option>
          <option value="60">60</option>
        </Field>
      </div>
      {quantityOfRendering && (
        <div>
          <h6>show {quantityOfRendering} results</h6>
        </div>
      )}
    </form>
  );
};

SelectingFormValuesForm = reduxForm({
  form: "selectingFormValues",
})(SelectingFormValuesForm);

const selector = formValueSelector("selectingFormValues");
SelectingFormValuesForm = connect((state) => {
  const selectedTypeOfSort = selector(state, "sortBy");
  const quantityOfRendering = selector(state, "quantityOfRendering");
  return {
    selectedTypeOfSort,
    quantityOfRendering,
  };
})(SelectingFormValuesForm);

export default SelectingFormValuesForm;
