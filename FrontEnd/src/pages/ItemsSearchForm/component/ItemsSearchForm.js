import React from "react";
import Preloader from "../../../components/Preloader/index";

const ItemsSearchForm = ({ searchIsReady, searchResult, searchText }) => {
  return (
    <div className="m-5 p-5 container-fluid d-flex flex-wrap justify-content-center bg-light">
      {!searchIsReady ? (
        <Preloader />
      ) : (
        <div>
          {!searchResult ? (
            <p>Noting was found for request: {`"${searchText}"`} </p>
          ) : (
            <div>
              <p>Results of your request: {`"${searchText}"`} </p>
              <p>{!searchResult.title}</p>
              <img src={searchResult.image} alt="" />
              <p>Description: {searchResult.description}</p>
              <p>Price: {searchResult.price}</p>
              <p>Brand: {searchResult.brand}</p>
              <p>Genre: {searchResult.genre}</p>
              <p>Gaming System: {searchResult.gamingSystem}</p>
              <p>Game Rating: {searchResult.gameRating}</p>
              <p>Availability: {searchResult.availability}</p>
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default ItemsSearchForm;
