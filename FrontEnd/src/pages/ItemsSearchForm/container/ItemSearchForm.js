import React from "react";
import axiox from "axios";
import { connect } from "react-redux";
import ItemSearchForm from "../../ItemsSearchForm/component/ItemsSearchForm";

class ItemSearchFormContainer extends React.Component {
  render() {
    const { searchIsReady, searchResult, searchText } = this.props;
    return (
      <ItemSearchForm
        searchIsReady={searchIsReady}
        searchResult={searchResult}
        searchText={searchText}
      />
    );
  }
}

const mapStateToProps = ({ search }) => ({
  searchIsReady: search.searchIsReady,
  searchResult: search.searchResult,
  searchText: search.searchText,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ItemSearchFormContainer);
