import React from "react";
import Preloader from "../../../components/Preloader/index";
import CardElectronics from "../../CardPageGames/index";
import SidebarComponent from "../../Sidebar/index";
import WellcomePageProfile from "../../WellcomePageProfile/container/WellcomePageProfile";
import CardWomans from "../../CardPageWomans/index";

const Results = (props) => {
  let pagesCount = Math.ceil(props.totalProductCount / props.limit);
  let pages = [];

  for (let i = 1; i <= pagesCount; i++) {
    pages.push(i);
  }

  return (
    <div>
      <div className="d-flex justify-content-start ">
        <WellcomePageProfile />
      </div>

      <div className="mb-5 pl-5 pr-5 d-flex  justify-content-between bg-light">
        <div>
          {pages.map((p, i) => {
            return (
              <>
                <span
                  style={{
                    margin: "10px",
                    cursor: "pointer",
                    fontWeight: "bold",
                  }}
                  key={i}
                  className={props.currentPage === p && "selected-Page"}
                  onClick={() => {
                    props.onPageChanged(p);
                  }}
                >
                  {p}
                </span>
              </>
            );
          })}
          <span>
            {" "}
            Showing {props.limit} of {props.totalProductCount} Results{" "}
          </span>
        </div>
        <div className="d-flex">
          <form>
            <select
              onChange={(e) => {
                props.onSortByChanged(e.target.value);
              }}
            >
              <option value="">Select sort displaying...</option>
              <option value="Price Hight To Low">Price Hight To Low</option>
              <option value="Price Low To Hight">Price Low To Hight</option>
              <option value="Best Matches">Best Matches</option>
              <option value="Most Popular">Most Popular</option>
              <option value="Brand">Brand</option>
              <option value="Top Sellers">Top Sellers</option>
              <option value="Best Matches">Best Matches</option>
              <option value="Product Name A-Z">Product Name A-Z</option>
              <option value="Product Name Z-A">Product Name Z-A</option>
            </select>
          </form>

          <form>
            <select
              onChange={(e, currentPage) => {
                props.onLimitChanged(e.target.value, currentPage);
              }}
            >
              <option value="">Select page</option>
              {/* поставити перевірку на кількість товарів всього. Якщо менше, то не відображати відповідну опцію  */}
              <option value="6">6</option>
              <option value="12">12</option>
              <option value="24">24</option>
              <option value="36">36</option>
              <option value="48">48</option>
              <option value="60">60</option>
            </select>
          </form>
        </div>
      </div>

      <div className=" d-flex ">
        <div className="p-4 d-flex flex-column border">
          <SidebarComponent
            ColorArrey={props.ColorArrey}
            SizeArrey={props.SizeArrey}
            PriceArrey={props.PriceArrey}
            GamingSystemArray={props.GamingSystemArray}
            categoryPriceName={props.categoryPriceName}
            categorySizeName={props.categorySizeName}
            categoryColorName={props.categoryColorName}
            categoryBrandName={props.categoryBrandName}
            categoryGamingSystemName={props.categoryGamingSystemName}
            CategoriesArray={props.CategoriesArray} //!
            BrandArray={props.BrandArray}
            onColorChanged={props.onColorChanged}
            onSizeChanged={props.onSizeChanged}
            onBrandChanged={props.onBrandChanged}
            onGamingSystemChanged={props.onGamingSystemChanged}
            onPriceRangeChanged={props.onPriceRangeChanged}
            onMinChanged={props.onMinChanged}
            onCategoryChanged={props.onCategoryChanged}
            onMaxChanged={props.onMaxChanged}
            selectedRange={props.selectedRange}
            selectedBrand={props.selectedBrand}
            selectedGS={props.selectedGS}
            parentCategory={props.parentCategory}
            subCategory={props.subCategory}
            sortingItems={props.sortingItems}
            isCategorySorting={props.isCategorySorting}
            sortingBrandItems={props.sortingBrandItems}
            isBrandSorting={props.isBrandSorting}
            sortingSizeItems={props.sortingSizeItems}
            isSizeSorting={props.isSizeSorting}
            sortingColorItems={props.sortingColorItems}
            isColorSorting={props.isColorSorting}
            sortingGSItems={props.sortingGSItems}
            isGS={props.isGS}
          />
        </div>
        <div>
          {" "}
          <div className=" d-flex flex-wrap p-3 border justify-content-around">
            {!props.isReady ? (
              <Preloader />
            ) : (
              props.result.map((item, i) => (
                <div key={i}>
                  <div className="col-md-1">
                    {props.parentCategory === "electronics" && (
                      <CardElectronics
                        className="flex-fill"
                        title={item.title}
                        link={item.image ? item.image : item.defaultImage}
                        price={item.price}
                        id={item._id}
                        onItemClick={props.onItemClick}
                        currentItemDetails={props.currentItemDetails}
                      />
                    )}
                    {props.parentCategory === "womans" && (
                      <CardWomans
                        className="flex-fill"
                        title={item.brand}
                        link={item.images ? item.images[0] : item.defaultImage}
                        price={item.details.price}
                        id={item._id}
                        onItemClick={props.onItemClick}
                        currentItemDetails={props.currentItemDetails}
                      />
                    )}
                  </div>
                </div>
              ))
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Results;
