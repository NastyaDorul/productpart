import React from "react";
import axios from "axios";
import { connect } from "react-redux";
import {
  setSearchResult,
  setCurrentPage,
  setPageLimit,
  setTotalProductCount,
  setCurrentTypeOfSort,
  setColorFilter,
  setSizeFilter,
  setPriceRange,
  setBrandFilter,
  setGSFilter,
  setMin,
  setMax,
  setItemIdTitlePriceImg,
  setSubCategory,
  setParentCategory,
} from "../../../store/actions/Results";

import Results from "../component/Results";

function handleErrors(err) {
  if (err.response) {
    console.log("Error with response", err.response.status);
  } else if (err.request) {
    console.log("Error with request");
  } else {
    console.log("Error", err.message);
  }
}

class ResultContainer extends React.Component {
  componentDidMount() {
    const {
      setSearchResult,
      setTotalProductCount,
      limit,
      currentPage,
      parentCategory,
      totalProductCount,
      setCurrentTypeOfSort,
      setPageLimit,
      selectedTypeOfSort,
      setCurrentPage,
    } = this.props;

    console.log(limit, currentPage, selectedTypeOfSort, parentCategory);

    axios
      .get(
        `http://localhost:5000/api/products/category/${parentCategory
          .split(" ")
          .join(
            ""
          )}/allproducts?page=${currentPage}&limit=${limit}&sortBy=${selectedTypeOfSort}`
      )
      .then((res) => {
        setSearchResult(res.data.result);
        setTotalProductCount(res.data.total);
        setPageLimit(limit);
        setCurrentPage(currentPage);
      })
      .catch(handleErrors);
  }

  onLimitChanged = (limit) => {
    this.props.setPageLimit(limit);
    console.log(limit, this.props.currentPage, this.props.selectedTypeOfSort);
    axios
      .get(
        `http://localhost:5000/api/products/category/electronics/gaming-Games?page=${this.props.currentPage}&limit=${limit}&sortBy=${this.props.selectedTypeOfSort}&color=${this.props.selectedColorFilter}&size=${this.props.selectedSizeFilter}`
      )
      .then((res) => {
        this.props.setSearchResult(res.data.result);
        console.log(res.data.result);
      })
      .catch(handleErrors);
  };

  onSortByChanged = (selectedTypeOfSort) => {
    this.props.setCurrentTypeOfSort(selectedTypeOfSort);
    console.log(selectedTypeOfSort, this.props.limit, this.props.currentPage);
    axios
      .get(
        `http://localhost:5000/api/products/category/electronics/gaming-Games?page=${this.props.currentPage}&limit=${this.props.limit}&sortBy=${selectedTypeOfSort}&color=${this.props.selectedColorFilter}&size=${this.props.selectedSizeFilter}`
      )
      .then((res) => {
        this.props.setSearchResult(res.data.result);
        console.log(res.data.result);
      })
      .catch(handleErrors);
  };

  onPageChanged = (pageNumber) => {
    this.props.setCurrentPage(pageNumber);
    this.props.setPageLimit(this.props.limit);
    console.log(pageNumber, this.props.limit);
    axios
      .get(
        `http://localhost:5000/api/products/category/electronics/gaming-Games?page=${pageNumber}&limit=${this.props.limit}&sortBy=${this.props.selectedTypeOfSort}&color=${this.props.selectedColorFilter}&size=${this.props.selectedSizeFilter}`
      )
      .then((res) => {
        this.props.setSearchResult(res.data.result);
      })
      .catch(handleErrors);
  };

  onColorChanged = (selectedColorFilter) => {
    this.props.setColorFilter(selectedColorFilter);
    console.log(selectedColorFilter);
    axios
      .get(
        `http://localhost:5000/api/products/category/electronics/gaming-Games?page=${this.props.currentPage}&limit=${this.props.limit}&sortBy=${this.props.selectedTypeOfSort}&color=${selectedColorFilter}&size=${this.props.selectedSizeFilter}`
      )
      .then((res) => {
        this.props.setSearchResult(res.data.result);
        console.log(res.data.result);
      })
      .catch(handleErrors);
  };
  onSizeChanged = (selectedSizeFilter) => {
    this.props.setSizeFilter(selectedSizeFilter);
    console.log(selectedSizeFilter);
    axios
      .get(
        `http://localhost:5000/api/products/category/electronics/gaming-Games?page=${this.props.currentPage}&limit=${this.props.limit}&sortBy=${this.props.selectedTypeOfSort}&color=${this.props.selectedColorFilter}&size=${selectedSizeFilter}`
      )
      .then((res) => {
        this.props.setSearchResult(res.data.result);
        console.log(res.data.result);
      })
      .catch(handleErrors);
  };

  onPriceRangeChanged = (selectedRange, min, max) => {
    this.props.setPriceRange(selectedRange);
    console.log(selectedRange);
    const test = [];
    for (let key in selectedRange) {
      if (selectedRange[key] === true) {
        test.push(key);
        // min = String(test).split(" ")[0];
        // max = String(test).split(" ")[2];
        // console.log(min, max);
      }
    }
    let test1 = String(test);
    //let test1 = String(test).splice(2, [, 1, "&priceRange="]);do not working
    console.log(test1);

    axios
      .get(
        `http://localhost:5000/api/products/category/electronics/gaming-Games?page=${
          this.props.currentPage
        }&limit=${this.props.limit}&sortBy=${
          this.props.selectedTypeOfSort
        }&color=${this.props.selectedColorFilter}&size=${
          this.props.selectedSizeFilter
        }&priceRange=${String(test)}&category=${this.props.selectedCategory}`
      )
      .then((res) => {
        this.props.setSearchResult(res.data.result);
        console.log(res.data.result);
      })
      .catch(handleErrors);
  };
  onBrandChanged = (selectedBrand) => {
    this.props.setBrandFilter(selectedBrand);
    console.log(selectedBrand);
  };

  onGamingSystemChanged = (selectedGS) => {
    this.props.setGSFilter(selectedGS);
    console.log(selectedGS);
  };

  onCategoryChanged = (subCategory) => {
    this.props.setSubCategory(subCategory);
    console.log(subCategory);
    axios
      .get(
        `http://localhost:5000/api/products/category/${this.props.parentCategory}/gaming-Games?page=${this.props.currentPage}&limit=${this.props.limit}&sortBy=${this.props.selectedTypeOfSort}&color=${this.props.selectedColorFilter}&size=${this.props.selectedSizeFilter}&subCategory=${subCategory}`
      )
      .then((res) => {
        this.props.setSearchResult(res.data.result);
        console.log(res.data.result);
      })
      .catch(handleErrors);
  };
  onMinChanged = (categoryPriceMin) => {
    this.props.setMin(categoryPriceMin);
    console.log(categoryPriceMin);
  };
  onMaxChanged = (categoryPriceMax) => {
    this.props.setMax(categoryPriceMax);
    console.log(categoryPriceMax);
  };
  onItemClick = (currentItemDetails) => {
    this.props.setItemIdTitlePriceImg(currentItemDetails);
  };

  ColorArrey = [
    { arr: "clear" },
    { arr: "black" },
    { arr: "navy" },
    { arr: "blue" },
    { arr: "brown" },
    { arr: "green" },
    { arr: "grey" },
    { arr: "orange" },
    { arr: "pink" },
    { arr: "purple" },
    { arr: "red" },
    { arr: "white" },
    { arr: "yellow" },
  ];

  categoryColorName = "color";
  categorySizeName = "size";
  categoryPriceName = "price";
  categoryBrandName = "brand";
  categoryGamingSystemName = "gaming system";

  PriceArrey = [
    { pmin: 20, pmax: 49.99 },
    { pmin: 50, pmax: 99.99 },
    { pmin: 100, pmax: 499.99 },
  ];

  GamingSystemArray = [
    { arr: "Sony PSP" },
    { arr: "Wii" },
    { arr: "Nintendo DS" },
    { arr: "SonyPS3" },
    { arr: "X-Box 360" },
    { arr: "No" },
  ];

  render() {
    const {
      result,
      isReady,
      totalProductCount,
      limit,
      currentPage,
      selectedRange,
      selectedBrand,
      selectedGS,
      currentItemDetails,
      parentCategory,
      subCategory,
      sortingItems,
      sortingBrandItems,
      isBrandSorting,
      isCategorySorting,
      sortingSizeItems,
      isSizeSorting,
      sortingColorItems,
      isColorSorting,
      sortingGSItems,
      isGS,
    } = this.props;

    return (
      <Results
        totalProductCount={totalProductCount}
        limit={limit}
        currentPage={currentPage}
        isReady={isReady}
        result={result}
        onPageChanged={this.onPageChanged}
        onLimitChanged={this.onLimitChanged}
        onSortByChanged={this.onSortByChanged}
        ColorArrey={sortingColorItems}
        categoryColorName={this.categoryColorName}
        SizeArrey={sortingSizeItems}
        categorySizeName={this.categorySizeName}
        PriceArrey={this.PriceArrey}
        categoryPriceName={this.categoryPriceName}
        CategoriesArray={sortingItems}
        BrandArray={sortingBrandItems}
        categoryBrandName={this.categoryBrandName}
        //GamingSystemArray={this.GamingSystemArray}
        GamingSystemArray={sortingGSItems}
        categoryGamingSystemName={this.categoryGamingSystemName}
        onColorChanged={this.onColorChanged}
        onSizeChanged={this.onSizeChanged}
        onPriceRangeChanged={this.onPriceRangeChanged}
        onBrandChanged={this.onBrandChanged}
        onGamingSystemChanged={this.onGamingSystemChanged}
        onCategoryChanged={this.onCategoryChanged}
        onMinChanged={this.onMinChanged}
        onMaxChanged={this.onMaxChanged}
        onItemClick={this.onItemClick}
        selectedRange={selectedRange}
        selectedBrand={selectedBrand}
        selectedGS={selectedGS}
        currentItemDetails={currentItemDetails}
        parentCategory={parentCategory}
        subCategory={subCategory}
        sortingItems={sortingItems}
        isCategorySorting={isCategorySorting}
        sortingBrandItems={sortingBrandItems}
        isBrandSorting={isBrandSorting}
        sortingSizeItems={sortingSizeItems}
        isSizeSorting={isSizeSorting}
        sortingColorItems={sortingColorItems}
        isColorSorting={isColorSorting}
        sortingGSItems={sortingGSItems}
        isGS={isGS}
      />
    );
  }
}

const mapStateToProps = ({ result }) => ({
  result: result.result,
  isReady: result.isReady,
  totalProductCount: result.totalProductCount,
  pageSize: result.pageSize,
  currentPage: result.currentPage,
  currentPageResult: result.currentPageResult,
  limit: result.limit,
  selectedTypeOfSort: result.selectedTypeOfSort,
  selectedColorFilter: result.selectedColorFilter,
  selectedSizeFilter: result.selectedSizeFilter,
  selectedRange: result.selectedRange,
  selectedBrand: result.selectedBrand,
  selectedGS: result.selectedGS,
  subCategory: result.subCategory, //з компонента Navbar
  categoryPriceMin: result.categoryPriceMin,
  categoryPriceMax: result.categoryPriceMax,
  currentItemDetails: result.currentItemDetails,
  parentCategory: result.parentCategory, //з компонента WellcomePage
  sortingItems: result.sortingItems,
  isCategorySorting: result.isCategorySorting,
  sortingBrandItems: result.sortingBrandItems,
  isBrandSorting: result.isBrandSorting,
  sortingSizeItems: result.sortingSizeItems,
  isSizeSorting: result.isSizeSorting,
  sortingColorItems: result.sortingColorItems,
  isColorSorting: result.isColorSorting,
  sortingGSItems: result.sortingGSItems,
  isGS: result.isGS,
});

const mapDispatchToProps = (dispatch) => ({
  setSearchResult: (result) => {
    dispatch(setSearchResult(result));
  },
  setCurrentPage: (currentPage) => {
    dispatch(setCurrentPage(currentPage));
  },

  setPageLimit: (limit) => {
    dispatch(setPageLimit(limit));
  },
  setTotalProductCount: (total) => {
    dispatch(setTotalProductCount(total));
  },
  setCurrentTypeOfSort: (selectedTypeOfSort) => {
    dispatch(setCurrentTypeOfSort(selectedTypeOfSort));
  },
  setColorFilter: (selectedColorFilter) => {
    dispatch(setColorFilter(selectedColorFilter));
  },
  setSizeFilter: (selectedSizeFilter) => {
    dispatch(setSizeFilter(selectedSizeFilter));
  },
  setPriceRange: (selectedRange) => {
    dispatch(setPriceRange(selectedRange));
  },
  setBrandFilter: (selectedBrand) => {
    dispatch(setBrandFilter(selectedBrand));
  },
  setGSFilter: (selectedGS) => {
    dispatch(setGSFilter(selectedGS));
  },

  setMin: (categoryPriceMin) => {
    dispatch(setMin(categoryPriceMin));
  },
  setMax: (categoryPriceMax) => {
    dispatch(setMax(categoryPriceMax));
  },
  setItemIdTitlePriceImg: (currentItemDetails) => {
    dispatch(setItemIdTitlePriceImg(currentItemDetails));
  },
  setSubCategory: (subCategory) => {
    dispatch(setSubCategory(subCategory));
  },
  setParentCategory: (parentCategory) => {
    dispatch(setParentCategory(parentCategory));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ResultContainer);
