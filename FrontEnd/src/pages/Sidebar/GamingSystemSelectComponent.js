import React from "react";

const GamingSystemSelectComponent = (props) => {
  return (
    <div>
      <div>
        <div
          className="container"
          data-toggle="tooltip"
          data-placement="left"
          title={props.categoryName}
          style={{
            width: "150px",
            height: "30px",
            border: "none",
            backgroundColor: "white",
            margin: "2px",
          }}
        >
          <input
            type="checkbox"
            checked={props.selectedPriceMax} //other value
            className="form-check-input"
            value={props.categoryName}
            onChange={(e) => {
              console.log(props);
              let object = Object.assign(props.selectedGS);
              console.log(object);
              object[e.currentTarget.value] = e.currentTarget.checked;
              props.onGamingSystemChanged(object);
            }}
          />
          {props.categoryName}
        </div>
      </div>
    </div>
  );
};

export default GamingSystemSelectComponent;
