import React from "react";

const CategoriesSelectComponent = (props) => {
  return (
    <div>
      <div
        className="container"
        title={props.category}
        style={{
          width: "180px",
          height: "30px",
          margin: "2px",
        }}
        onClick={(e, subCategory) =>
          props.onCategoryChanged(e.currentTarget.title, subCategory)
        }
      >
        <p>{props.category}</p>
      </div>
    </div>
  );
};

export default CategoriesSelectComponent;
