import React from "react";

const PriceSelectComponent = (props) => {
  return (
    <div>
      <div>
        <div
          className="container"
          data-toggle="tooltip"
          data-placement="left"
          title={props.categoryPriceMax}
          style={{
            width: "150px",
            height: "30px",
            border: "none",
            backgroundColor: "white",
            margin: "2px",
          }}
        >
          <input
            type="checkbox"
            checked={props.selectedPriceMax}
            className="form-check-input"
            value={`${props.categoryPriceMin} to ${props.categoryPriceMax}`}
            onChange={(e) => {
              console.log(props);
              let object = Object.assign(props.selectedRange);
              object[e.currentTarget.value] = e.currentTarget.checked;
              props.onPriceRangeChanged(object);
              props.onMinChanged(props.categoryPriceMin);
              props.onMaxChanged(props.categoryPriceMax);
            }}
          />
          {`${props.categoryPriceMin} to ${props.categoryPriceMax}`}
        </div>
      </div>
    </div>
  );
};

export default PriceSelectComponent;
