import React from "react";
import { Link } from "react-router-dom";
import ColorSelectComponent from "./ColorSelectComponent";
import SizeSelectComponent from "./SizeSelectComponent";
import PriceSelectComponent from "./PriceSelectComponent";
import BrandSelectComponent from "./BrandSelectComponent";
import GamingSystemSelectComponent from "./GamingSystemSelectComponent";
import CategoriesSelectComponent from "./CategoriesSelectComponent";
import SearchFormContainer from "../SearchForm/containers/SearchForm";
import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Preloader from "../../components/Preloader";

const SidebarComponent = (
  props,
  { CategoryItems, BrandItems, SizeItems, ColorItems, GamingSystemItems }
) => {
  console.log(props.sortingSizeItems);

  {
    props.sortingItems !== null &&
      (CategoryItems = props.CategoriesArray.map((category, index) => {
        return (
          <div key={index} id="category" className="collapse">
            <CategoriesSelectComponent
              category={category}
              onCategoryChanged={props.onCategoryChanged}
            />
          </div>
        );
      }));
  }
  {
    props.sortingColorItems !== null &&
      (ColorItems = props.ColorArrey.map((elem, index) => {
        return (
          <div key={index} id="color" className="collapse">
            <ColorSelectComponent
              categoryColors={elem}
              onColorChanged={props.onColorChanged}
            />
          </div>
        );
      }));
  }

  {
    props.sortingSizeItems !== null &&
      (SizeItems = props.SizeArrey.map((size, index) => {
        return (
          <div key={index} id="size" className="collapse">
            <SizeSelectComponent
              categorySize={size}
              onSizeChanged={props.onSizeChanged}
            />
          </div>
        );
      }));
  }

  let PriceItems = props.PriceArrey.map((price, index) => {
    return (
      <div key={index} id="price" className="collapse">
        <PriceSelectComponent
          categoryPriceMax={price.pmax}
          categoryPriceMin={price.pmin}
          onPriceRangeChanged={props.onPriceRangeChanged}
          onMinChanged={props.onMinChanged}
          selectedRange={props.selectedRange}
          onMaxChanged={props.onMaxChanged}
        />
      </div>
    );
  });

  {
    props.sortingBrandItems !== null &&
      (BrandItems = props.BrandArray.map((brand, index) => {
        return (
          <div key={index} id="brand" className="collapse">
            <BrandSelectComponent
              categoryName={brand}
              onBrandChanged={props.onBrandChanged}
              selectedBrand={props.selectedBrand}
            />
          </div>
        );
      }));
  }

  {
    props.sortingGSItems !== null &&
      (GamingSystemItems = props.GamingSystemArray.map((system, index) => {
        return (
          <div key={index} id="system" className="collapse">
            <GamingSystemSelectComponent
              categoryName={system}
              onGamingSystemChanged={props.onGamingSystemChanged}
              selectedGS={props.selectedGS}
            />
          </div>
        );
      }));
  }

  return (
    <div>
      <div className="card" style={{ width: "220px" }}>
        <div className="card-header" />
        <nav className="navbar">
          {" "}
          <SearchFormContainer />
        </nav>
      </div>

      {!props.isCategorySorting ? (
        <Preloader />
      ) : (
        <div className="card" style={{ width: "220px" }}>
          <div className="card-header">
            <Link
              to=""
              className="card-link"
              style={{ fontWeight: "bold", color: "black" }}
              data-toggle="collapse"
              data-target="#category"
            >
              <FontAwesomeIcon
                icon={faCheckCircle}
                size="x"
                style={{ color: "darkgreen", marginRight: "10px" }}
              />
              {props.parentCategory.toUpperCase()}{" "}
            </Link>{" "}
          </div>
          <Link
            to={`/products/subCategory?${props.subCategory}`}
            style={{ textDecoration: "none", color: "black" }}
          >
            {" "}
            <nav className="navbar">{CategoryItems}</nav>
          </Link>
        </div>
      )}

      {props.sortingColorItems == null ? (
        <></>
      ) : (
        <div>
          {!props.isColorSorting ? (
            <Preloader />
          ) : (
            <div className="card" style={{ width: "220px" }}>
              <div className="card-header">
                <Link
                  to=""
                  className="card-link"
                  style={{ fontWeight: "bold", color: "black" }}
                  data-toggle="collapse"
                  data-target="#color"
                >
                  <FontAwesomeIcon
                    icon={faCheckCircle}
                    size="x"
                    style={{ color: "darkgreen", marginRight: "10px" }}
                  />
                  {props.categoryColorName.toUpperCase()}
                </Link>{" "}
              </div>
              <Link to={`/products/subCategory?${props.subCategory}`}>
                {" "}
                <nav className="navbar">{ColorItems}</nav>
              </Link>
            </div>
          )}
        </div>
      )}

      {props.sortingSizeItems == null ? (
        <></>
      ) : (
        <div>
          {!props.isSizeSorting ? (
            <Preloader />
          ) : (
            <div className="card" style={{ width: "220px" }}>
              <div className="card-header">
                <Link
                  to=""
                  className="card-link"
                  style={{ fontWeight: "bold", color: "black" }}
                  data-toggle="collapse"
                  data-target="#size"
                >
                  <FontAwesomeIcon
                    icon={faCheckCircle}
                    size="x"
                    style={{ color: "darkgreen", marginRight: "10px" }}
                  />
                  {props.categorySizeName.toUpperCase()}
                </Link>{" "}
              </div>
              <nav className="navbar">{SizeItems}</nav>
            </div>
          )}
        </div>
      )}

      <div className="card" style={{ width: "220px" }}>
        <div className="card-header">
          <Link
            to=""
            className="card-link"
            style={{ fontWeight: "bold", color: "black" }}
            data-toggle="collapse"
            data-target="#price"
          >
            <FontAwesomeIcon
              icon={faCheckCircle}
              size="x"
              style={{ color: "darkgreen", marginRight: "10px" }}
            />
            {props.categoryPriceName.toUpperCase()}
          </Link>{" "}
        </div>
        <nav className="navbar">{PriceItems}</nav>
      </div>

      {props.sortingBrandItems == null ? (
        <></>
      ) : (
        <div>
          {!props.isBrandSorting ? (
            <Preloader />
          ) : (
            <div className="card" style={{ width: "220px" }}>
              <div className="card-header">
                <Link
                  to=""
                  className="card-link"
                  style={{ fontWeight: "bold", color: "black" }}
                  data-toggle="collapse"
                  data-target="#brand"
                >
                  <FontAwesomeIcon
                    icon={faCheckCircle}
                    size="x"
                    style={{ color: "darkgreen", marginRight: "10px" }}
                  />
                  {props.categoryBrandName.toUpperCase()}
                </Link>{" "}
              </div>
              <nav className="navbar">{BrandItems}</nav>
            </div>
          )}
        </div>
      )}

      {props.sortingGSItems == null ? (
        <></>
      ) : (
        <div>
          {!props.isGS ? (
            <Preloader />
          ) : (
            <div className="card" style={{ width: "220px" }}>
              <div className="card-header">
                <Link
                  to=""
                  className="card-link"
                  style={{ fontWeight: "bold", color: "black" }}
                  data-toggle="collapse"
                  data-target="#system"
                >
                  <FontAwesomeIcon
                    icon={faCheckCircle}
                    size="x"
                    style={{ color: "darkgreen", marginRight: "10px" }}
                  />
                  {props.categoryGamingSystemName.toUpperCase()}
                </Link>{" "}
              </div>
              <Link
                to={`/products/subCategory?${props.subCategory}`}
                style={{ textDecoration: "none", color: "black" }}
              >
                <nav className="navbar">{GamingSystemItems}</nav>
              </Link>
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default SidebarComponent;
