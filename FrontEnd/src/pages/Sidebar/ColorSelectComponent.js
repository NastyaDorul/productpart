import React from "react";

const ColorSelectComponent = (props) => {
  return (
    <div>
      <div
        data-toggle="tooltip"
        data-placement="right"
        title={props.categoryColors}
        style={{
          width: "40px",
          height: "40px",
          border: "1px solid lightgrey",
          backgroundColor: props.categoryColors,
          borderRadius: "10px",
          margin: "2px",
        }}
        onClick={(e, selectedColorFilter) => {
          props.onColorChanged(e.target.title, selectedColorFilter);
        }}
      ></div>
    </div>
  );
};

export default ColorSelectComponent;
