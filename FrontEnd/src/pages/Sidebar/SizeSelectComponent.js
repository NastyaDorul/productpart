import React from "react";

const SizeSelectComponent = (props) => {
  return (
    <div>
      <div
        className="container justify-content-center"
        data-toggle="tooltip"
        data-placement="right"
        title={props.categorySize}
        style={{
          width: "50px",
          height: "30px",
          border: "1px solid grey",
          backgroundColor: "white",
          borderRadius: "5px",
          margin: "2px",
        }}
        onClick={(e, selectedSizeFilter) =>
          props.onSizeChanged(e.currentTarget.title, selectedSizeFilter)
        }
      >
        <p
          style={{
            color: "grey",
            fontSize: 16,
            fontFamily: "Arial",
          }}
        >
          {props.categorySize}
        </p>
      </div>
    </div>
  );
};

export default SizeSelectComponent;
