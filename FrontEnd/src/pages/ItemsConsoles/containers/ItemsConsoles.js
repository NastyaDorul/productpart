import { connect } from "react-redux";

import ItemsConsoles from "../components/ItemsConsoles";

const mapStateToProps = ({}) => ({});
const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ItemsConsoles);
