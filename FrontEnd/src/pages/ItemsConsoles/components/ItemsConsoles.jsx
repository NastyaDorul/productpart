import React from "react";
import axios from "axios";
import Preloader from "../../../components/Preloader/index";

class ItemsConsoles extends React.Component {
  componentDidMount() {
    const { setItemId } = this.props;
    const id = this.props.match.params.id || "";
    //const path = this.props.history.push(`${id}`); //testing

    console.log(id);
    console.log(this.props);
    setItemId(id);
    axios
      .get(
        `http://localhost:5000/api/products/category/electronics/gaming-Game-consoles/search-By/${id}`
      )
      .then((res) => {
        setItemId(res.data);
        console.log(res);
      });
  }
  render() {
    const { item, isReady } = this.props;

    console.log(item, isReady);
    return (
      <div className="m-4 d-flex justify-items-center">
        <ul>
          <h3>Information about item</h3>
          {!isReady ? (
            <Preloader />
          ) : (
            <li>
              <h3>{item.title}</h3>
              <h3>Availability: {item.availability}</h3>
              <img src={item.image} alt="availability" />

              <h3>Brand: {item.brand}</h3>
              <h3>Extended Warranty:{item.extendedWarranty}</h3>
              <h3>Price:{item.price} </h3>
              <h3>{item.description}</h3>
            </li>
          )}
        </ul>
      </div>
    );
  }
}
export default ItemsConsoles;
