import React from "react";
import axios from "axios";
import { connect } from "react-redux";
import ItemsDetalization from "../components/ItemsDetalization";
import { setItemDetails } from "../../../store/actions/Results";

class ItemsDetalizationContainer extends React.Component {
  componentDidMount() {
    const { id, parentCategory } = this.props;
    console.log(id);
    {
      axios
        .get(
          `http://localhost:5000/api/products/category/${parentCategory}/search-By/${id}`
        )
        .then((res) => {
          console.log(res.data);
          this.props.setItemDetails(res.data);
        })
        .catch(function (error) {
          console.log(error, "error");
        });
    }
  }

  render() {
    return <ItemsDetalization {...this.props} />;
  }
}

const mapStateToProps = ({ result }) => ({
  id: result.currentItemDetails.id,
  itemDetails: result.itemDetails,
  itemDetailsIsReady: result.itemDetailsIsReady,
  parentCategory: result.parentCategory,
});
const mapDispatchToProps = (dispatch) => ({
  setItemDetails: (itemDetails) => {
    dispatch(setItemDetails(itemDetails));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ItemsDetalizationContainer);
