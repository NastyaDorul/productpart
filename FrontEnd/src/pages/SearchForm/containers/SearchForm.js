import React from "react";
import axios from "axios";
import { connect } from "react-redux";
import {
  setSearchQuery,
  setSearchQueryResult,
} from "../../../store/actions/searchForm";
import SearchForm from "../component/SearchForm";

class SearchFormContainer extends React.Component {
  componentDidMount() {
    console.log(this.props);
  }

  onSearchTextChanged = (searchText) => {
    console.log(searchText);
    axios
      .get(
        `http://localhost:5000/api/products/category/${this.props.parentCategory}/search/products/${searchText}`
      )
      .then((res) => {
        this.props.setSearchQueryResult(res.data);
        console.log(res.data);
      })
      .catch(function (error) {
        console.log(error, "error");
      });
  };

  render() {
    const {
      searchIsReady,
      searchResult,
      searchText,
      setSearchQuery,
    } = this.props;
    return (
      <SearchForm
        onSearchTextChanged={this.onSearchTextChanged}
        searchIsReady={searchIsReady}
        searchResult={searchResult}
        searchText={searchText}
        setSearchQuery={setSearchQuery}
      />
    );
  }
}

const mapStateToProps = ({ search, result }) => ({
  searchText: search.searchText,
  searchIsReady: search.searchIsReady,
  searchResult: search.searchResult,
  parentCategory: result.parentCategory,
});
const mapDispatchToProps = (dispatch) => ({
  setSearchQuery: (searchText) => {
    dispatch(setSearchQuery(searchText));
  },
  setSearchQueryResult: (searchResult) => {
    dispatch(setSearchQueryResult(searchResult));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchFormContainer);
