import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

const SearchForm = (props) => {
  console.log(props);
  return (
    <div>
      <input
        onChange={(e) => {
          let searchText = e.target.value;
          props.setSearchQuery(searchText); //переробити
        }}
        value={props.searchText}
        type="text"
        placeholder="Enter search-key..."
        style={{ width: "150px" }}
      />
      <button
        onClick={() => {
          console.log(props);
          if (props.searchText !== null) {
            props.onSearchTextChanged(props.searchText);
          }
        }}
      >
        <Link to={`/search/products/${props.searchText}`}>
          <FontAwesomeIcon icon={faSearch} style={{ color: "grey" }} />
        </Link>
      </button>
    </div>
  );
};

export default SearchForm;
