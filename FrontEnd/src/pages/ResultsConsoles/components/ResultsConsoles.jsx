import React from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Preloader from "../../../components/Preloader/index";
import Card from "../../../pages/CardPageConsoles/index";

class ResultsConsoles extends React.Component {
  componentDidMount() {
    const { setProducts } = this.props;
    axios
      .get(
        "http://localhost:5000/api/products/category/electronics/gaming-Game-consoles"
      )
      .then((res) => {
        setProducts(res.data);
        console.log(res.data);
      });
  }
  render() {
    const { products, isReady } = this.props;
    console.log(this.props);

    return (
      <div className="m-5 p-5 container-fluid d-flex flex-wrap justify-content-center bg-light">
        {!isReady ? (
          <Preloader />
        ) : (
          products.map((product, i) => (
            <div key={i} className="row">
              <div className="col-md-1">
                <Card
                  title={product.title}
                  link={product.image}
                  price={product.price}
                  id={product._id}
                />
              </div>
            </div>
          ))
        )}
      </div>
    );
  }
}
export default ResultsConsoles;
