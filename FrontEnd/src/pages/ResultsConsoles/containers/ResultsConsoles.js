import { connect } from "react-redux";
import { setProducts } from "../../../store/actions/products";
import ResultsConsoles from "../components/ResultsConsoles";

const mapStateToProps = ({ products }) => ({
  products: products.items,
  isReady: products.isReady
});
const mapDispatchToProps = dispatch => ({
  setProducts: products => dispatch(setProducts(products))
});

export default connect(mapStateToProps, mapDispatchToProps)(ResultsConsoles);
